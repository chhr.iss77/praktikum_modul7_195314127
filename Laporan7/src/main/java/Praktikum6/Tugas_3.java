package Praktikum6;

import java.util.Scanner;

public class Tugas_3 {
    
    public static void main(String args[]){
        String kataAsli;
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Input Kata : ");
        kataAsli = sc.next();
        String kataBalik = new StringBuffer(kataAsli).reverse().toString();
        System.out.println("Reverse Kata : "+kataBalik); 
        if(kataAsli.equals(kataBalik)){
         System.out.println("Palindrome");
        }else{
         System.out.println("not palindrome");
        }
    }
}
