package Praktikum6;

import java.util.Scanner;
import java.util.StringTokenizer;

public class Tugas_4 {
    public static void main(String args[]){
        Scanner scan = new Scanner(System.in);
        //get user
        System.out.println("Masukkan sebuah kalimat : ");
        String sentence = scan.nextLine();
        
        //proses user sentence
        StringTokenizer tokens = new StringTokenizer(sentence);
        
        System.out.println("Jumlah Kata : ");
        while(tokens.hasMoreTokens()){
            System.out.println(tokens.countTokens());
            break;
        }
    }
}
