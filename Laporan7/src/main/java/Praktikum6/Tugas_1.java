package Praktikum6;

import java.util.Scanner;


public class Tugas_1 {
    public static void main(String args[]){
        Scanner data = new Scanner(System.in);
        String huruf, ganti;
        
        System.out.println("Input kata : ");
        String input = data.next();
        
        input = input.replace("a", "4");
        input = input.replace("i", "1");
        input = input.replace("e", "3");
        input = input.replace("o", "0");
        input = input.replace("u", "11");
        
        System.out.println("Output :"+input);
        
    }
}
